#!/usr/bin/python3

"""
This code can process images of the Connect4 board.

Jonathan Piland
2 December 2023
"""

import rospy
from sensor_msgs.msg import Image
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.animation import FuncAnimation
from datetime import datetime
from dataclasses import dataclass
import numpy as np
from typing import List
import cv2
from cv_bridge import CvBridge
bridge = CvBridge()

class VideoCamera():

    def __init__(self) -> None:

        blank_image = np.zeros([800, 800, 3], dtype=np.uint8)
        self._current_image = blank_image
        rospy.Subscriber("ur5e/camera1/image_raw", Image, self._callback)

    def _callback(self, msg: Image):
        # change back to rgb8 if using Jonathan's CV function!!!!!
        self._current_image = bridge.imgmsg_to_cv2(msg, "bgr8")

    def get_snapshot(self):
        return self._current_image.astype(np.float32) / 255
    
    def get_cv_snapshot(self):
        return self._current_image


def get_column_played(column_heights: List[int], previous_snapshot: np.ndarray, current_snapshot: np.ndarray):
    """
    IMPORTANT NOTE: both images must be on the scale of 0 to 1.
    """

    if np.max(previous_snapshot) > 1:
        raise ValueError("Max of image is too large. Is it on the scale of 0 to 1?")
    if np.max(current_snapshot) > 1:
        raise ValueError("Max of image is too large. Is it on the scale of 0 to 1?")

    image = current_snapshot - previous_snapshot
    image = (image - np.min(image)) / (np.max(image) - np.min(image))

    if len(column_heights) !=  7:
        raise ValueError("Invalid length of column heights array")
    for height in column_heights:
        if (not (0 <= height <= 6)) or (int(height) != height):
            raise ValueError(f"Invalid column height: {height}")
    
    rectangles: List[List[RectangleBounds]] = []

    for column in range(7):
        rectangles.append([])
        for rect in range(6):
            rectangles[-1].append(RectangleBounds(
                125 + column * 88,
                610 - rect * 80,
                125 + column * 88 + 60,
                610 - rect * 80 + 60
            ))

    rectangles_of_interest = []

    for column in range(7):
        if column_heights[column] == 6:
            rectangles_of_interest.append(None)
            continue
        rectangles_of_interest.append(rectangles[column][column_heights[column]])

    deltas = []

    for roi in rectangles_of_interest:
        if roi is None:
            deltas.append(1e99) # large number that will not be the minimum
            continue
        average_change = np.average(image[roi.y1:roi.y2, roi.x1:roi.x2])
        deltas.append(average_change)

    # print(deltas)
    
    return 1 + np.argmin(deltas)


# CV Board Detection Code Using CV2

# Display image (remember, this only works in FastX!).
def imgshow(name,img):
    cv2.imshow(name,img)
    cv2.moveWindow(name,200,200)
    cv2.waitKey(0)

def get_board_state_using_cv2(snapshot: np.ndarray):
    # Read and process image.
    input_img = snapshot

    cv2.imwrite("test2.png", snapshot)
    # plt.imshow(snapshot)
    # plt.savefig(f"test.png")

    # Resize input image.
    new_width = 500
    img_h,img_w,_ = input_img.shape
    scale = new_width / img_w
    img_w = int(img_w * scale)
    img_h = int(img_h * scale)
    # See https://medium.com/@wenrudong/what-is-opencvs-inter-area-actually-doing-282a626a09b3 for what cv2.INTER_AREA
    # interpolation method means.
    input_img = cv2.resize(input_img, (img_w, img_h), interpolation=cv2.INTER_AREA)
    original_img = input_img.copy()
    # imgshow("Original Image (Resized)", original_img)

    # Apply bilateral filter.
    bilaterally_filtered_img = cv2.bilateralFilter(input_img, 15, 190, 190) 
    # imgshow("image after a bilateral filter was applied", bilaterally_filtered_img)

    # Outline the edges.
    edged_img = cv2.Canny(bilaterally_filtered_img, 75, 150) 
    # imgshow("image after applying edge detection", edged_img)

    # Find the spots.
    contours, _ = cv2.findContours(edged_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # edges to contours

    contour_list = []
    rect_list = []
    position_list = []

    for contour in contours:
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True) # Contour Polygons
        area = cv2.contourArea(contour)
        
        rect = cv2.boundingRect(contour) # Polygon bounding rectangles
        x_rect,y_rect,w_rect,h_rect = rect
        x_rect +=  w_rect/2
        y_rect += h_rect/2
        area_rect = w_rect*h_rect
        
        if ((len(approx) > 8) & (len(approx) < 23) & (area > 250) & (area_rect < (img_w*img_h)/5)) & (w_rect in range(h_rect-10,h_rect+10)): # Circle conditions
            contour_list.append(contour)
            position_list.append((x_rect,y_rect))
            rect_list.append(rect)

    img_circle_contours = original_img.copy()
    cv2.drawContours(img_circle_contours, contour_list,  -1, (0,255,0), thickness=1) # Display Circles
    for rect in rect_list:
        x,y,w,h = rect
        cv2.rectangle(img_circle_contours,(x,y),(x+w,y+h),(0,0,255),1)

    # imgshow('Circles Detected', img_circle_contours)

    # Interpolate Grid
    rows, cols = (6,7)
    mean_w = sum([rect[2] for r in rect_list]) / len(rect_list)
    mean_h = sum([rect[3] for r in rect_list]) / len(rect_list)
    position_list.sort(key = lambda x:x[0])
    max_x = int(position_list[-1][0])
    min_x = int(position_list[0][0])
    position_list.sort(key = lambda x:x[1])
    max_y = int(position_list[-1][1])
    min_y = int(position_list[0][1])
    grid_width = max_x - min_x
    grid_height = max_y - min_y
    col_spacing = int(grid_width / (cols-1))
    row_spacing = int(grid_height / (rows - 1))

    # Find Colour Masks
    img_hsv = cv2.cvtColor(input_img, cv2.COLOR_BGR2HSV) # Convert to HSV space

    lower_red = np.array([0, 80, 80])  # Lower range for red colour space
    upper_red = np.array([25, 255, 255])  # Upper range for red colour space
    mask_red = cv2.inRange(img_hsv, lower_red, upper_red)
    img_red = cv2.bitwise_and(input_img, input_img, mask=mask_red)
    # imgshow("Red Mask", img_red)

    lower_yellow = np.array([30, 150, 100])
    upper_yellow = np.array([60, 255, 255])
    mask_yellow = cv2.inRange(img_hsv, lower_yellow, upper_yellow)
    img_yellow = cv2.bitwise_and(input_img, input_img, mask=mask_yellow)
    # imgshow("Yellow Mask",img_yellow)

    # Identify Colours
    grid = np.zeros((rows, cols))
    id_red = 1
    id_yellow = -1
    img_grid_overlay = original_img.copy()
    img_grid = np.zeros([img_h,img_w,3], dtype=np.uint8)

    for x_i in range(0,cols):
        x = int(min_x + x_i * col_spacing)
        for y_i in range(0,rows):
            y = int(min_y + y_i * row_spacing)
            r = int((mean_h + mean_w)/5)
            img_grid_circle = np.zeros((img_h, img_w))
            cv2.circle(img_grid_circle, (x,y), r, (255,255,255),thickness=-1)
            img_res_red = cv2.bitwise_and(img_grid_circle, img_grid_circle, mask=mask_red)
            img_grid_circle = np.zeros((img_h, img_w))
            cv2.circle(img_grid_circle, (x,y), r, (255,255,255),thickness=-1)
            img_res_yellow = cv2.bitwise_and(img_grid_circle, img_grid_circle,mask=mask_yellow)
            cv2.circle(img_grid_overlay, (x,y), r, (0,255,0),thickness=1)
            if img_res_red.any() != 0:
                grid[y_i][x_i] = id_red
                cv2.circle(img_grid, (x,y), r, (0,0,255),thickness=-1)
            elif img_res_yellow.any() != 0 :
                grid[y_i][x_i] = id_yellow
                cv2.circle(img_grid, (x,y), r, (0,255,255),thickness=-1)
        
    # print('Grid Detected:\n', grid)
    # imgshow('Img Grid Overlay',img_grid_overlay)
    # imgshow('Img Grid',img_grid)

    return grid


@dataclass
class RectangleBounds:
    x1: int
    y1: int
    x2: int
    y2: int
